# Ansible

## Commands list

### Basic:

* Ping servers  
    `ansible rc -m ping`
    `ansible all -m ping`
* Run custom cli command  
    `ansible rc -m command -a "/bin/echo Hello world"`

### Playbooks:

* Run playbook  
    `ansible-playbook playbooks/apache.yml`  
    `ansible-playbook playbooks/apache.yml --ask-sudo-pass`   
    

